COMPONENTES 

modelo: 
	se le agrego a las entidades de empleado la propiedad companyId para realizas las operaciones que involucren su relacion 1:n,
	se asumio que un empleado no trabajaria para mas de una compania.

dao: 
	singleton gracias a @ApplicationScopep (a diferencia de los bean de logica y presentacion que son @RequestScoped)
	almacena los datos en una cache (hashtable multithreadsafe) se usa jaxb para la persistencia, poco eficiente al guardar con cada operacion de creacion.
	los datos persisten en un xml externo (resources dao.properties continen el path absoluto al archivo) 

logica: 
	se realizan algunas operaciones de validacion y filtrado simples, faltaria hacer mas verificaciones en los datos recibidos (se confia mucho en el cliente).
	accede al dao y es accedido por la capa de presentacion usando injeccion de dependencias con tags de javax-inject

presentacion: 
	jersey como implementacion de javax-rs
	para agregar empleado se logro presentar una URI uniforme para la familia de empledos gracias al soporte de polimorfismo de jackson 

cliente:
	simple html con algo de jQuery para realizar peticiones ajax, no maneja errores ni confirma operacion para add employee

excpciones:
	se definen especializaciones de WebApplicationException con su correspondiente respuesta http

TODOs

-remplzar las multiples excepciones con versiones custom  
-chequeo de logica de modelo: agregar un validador que recorra todos los campos y devuelva una lista para la ui  
-js refactor  


