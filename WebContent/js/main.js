// JavaScript Document
$(document).ready(
  function(){
	//loading company combobox data
		$.ajax({
			  dataType: "json",
			  url: "/suma-practico/rest/companies",
			  success: function (data, status){
				  options="";
				  data.forEach(function(entry){
					  options+="<option value="+entry.id+">"+entry.name+"</option>";
				  });
				  $('#companyCombBox').html(options);
			  }
		  });
		
		$('#companyCombBox').change(function(){	
		  $("#addEmployeeForm").attr('style','display: none;');
		  $("#listOfEmployees").attr('style','display: none;');
		  $("#employeeSearch").attr('style','display: none;');
		  $("#averageAge").attr('style','display: none;');
		});
		
	//menu buttons 	
		$("#addEmployee").click(function() {
			$("#addEmployeeForm").attr('style','');
			$("#listOfEmployees").attr('style','display: none;');
			$("#employeeSearch").attr('style','display: none;');
			$("#averageAge").attr('style','display: none;');});

		$("#listAllEmployees").click(function() {
			$("#addEmployeeForm").attr('style','display: none;');
			$("#listOfEmployees").attr('style','');
			$.ajax({
				  dataType: "json",
				  url: "/suma-practico/rest/employees/"+$('#companyCombBox').val(),
				  success: function (data, status){
					  employeeRows="";
					  data.forEach(function(entry){
//						  console.log(entry);
						  employeeRows+="<tr>";
						  employeeRows+="<td>"+entry.id+"</td>";
						  employeeRows+="<td>"+entry.firstName+"</td>";
						  employeeRows+="<td>"+entry.lastName+"</td>";
						  employeeRows+="<td>"+entry.age+"</td>";
						  employeeRows+="<td>"+(entry.hasOwnProperty('programingLanguage')?entry.programingLanguage+" developer":entry.designerType+" designer")+"</td>";
						  employeeRows+="</tr>";
					  });
					  $("#allEmployeesResult").html(employeeRows);
				  }
			  });
			$("#employeeSearch").attr('style','display: none;');
			$("#averageAge").attr('style','display: none;');});

		$("#searchEmployee").click(function() {
			$("#addEmployeeForm").attr('style','display: none;');
			$("#listOfEmployees").attr('style','display: none;');
			$("#employeeSearch").attr('style','');
			$("#averageAge").attr('style','display: none;');});

		$("#getAverageAge").click(function() {
			$("#addEmployeeForm").attr('style','display: none;');
			$("#listOfEmployees").attr('style','display: none;');
			$("#employeeSearch").attr('style','display: none;');
			$("#averageAge").attr('style','');
			$.ajax({
			  dataType: "json",
			  url: "/suma-practico/rest/employees/averageAge/"+$('#companyCombBox').val(),
			  success: function (data, status){
				  $("#avgAge").html("<h2>Average age: "+data+"</h2>");
			  }});	
		});
		
		//search employee form behavior
		$( "#idSearchForm" ).submit(function( event ) {
			  event.preventDefault();
			  $.ajax({
				  dataType: "json",
				  url: "/suma-practico/rest/employee/"+$('#idSearchInput').val(),
				  success: function (data, status){
					employee="";
					if(data && data.companyId==$('#companyCombBox').val()){
//						employee="id: "+data.id+" first name: "+data.firstName+" last name: "+data.lastName+" age: "+data.age;
//					
						employee+="<table class=\"table table-striped\"><thead><tr><th>Id</th><th>First Name</th><th>Last Name</th>";
						employee+="<th>Age</th><th>Role</th></tr></thead><tbody><tr>";  
						employee+="<td>"+data.id+"</td>";
						employee+="<td>"+data.firstName+"</td>";
						employee+="<td>"+data.lastName+"</td>";
						employee+="<td>"+data.age+"</td>";
						employee+="<td>"+(data.hasOwnProperty('programingLanguage')?data.programingLanguage+" developer":data.designerType+" designer")+"</td>";
						employee+="</tr></tbody></table>";
					}else{
						employee="No employee found";
					}
					$("#idSearchResult").html(employee);
				  }});	
			  
			});
		//add employee form behavior
		$( "#employeeTypeSelector" ).change(function( event ) {
			 
			 if( $( "#employeeTypeSelector" ).val() == "Developer"){
				 $(".designer-group").attr("style","display:none");
				 $(".developer-group").attr("style","");
			 }else{
				 $(".designer-group").attr("style","");
				 $(".developer-group").attr("style","display:none");
			 }		 
			});
		
		$( "#addEmployeeForm" ).submit(function( event ) {
			  event.preventDefault();
			  type= $( "#employeeTypeSelector" ).val().toLowerCase();
			  emp = {
					  id:parseInt($("#idInput").val()),
					  companyId:parseInt($('#companyCombBox').val()),
					  firstName:$("#fNameInput").val(),
					  lastName:$("#lNameInput").val(),
					  age:parseInt($("#ageInput").val()),
					  type:$( "#employeeTypeSelector" ).val().toLowerCase()
			  };
			  if (type == "designer"){
				  emp["designerType"]=$("#designTypeSelector").val();
			  } else {
				  emp["programingLanguage"]=$("#devLangSelector").val();
			  }
			  
			  $.ajax({
				  contentType: "application/json",
				  type: "PUT",
				  data:JSON.stringify(emp),
				  url: "/suma-practico/rest/employee/", 
				  success: function (data, status){
					  console.log(data);
					  console.log(status);
					
				  }});	
			  
			});
		
		
  });

