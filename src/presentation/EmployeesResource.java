package presentation;

import java.util.Collection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import logic.EmployeesManager;
import domain.Employee;

@RequestScoped
@Path("/employees/")
public class EmployeesResource {
	@Inject
	private EmployeesManager employeesManager;
	
	@GET
	@Path("{companyId}/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Employee> getAllEmployees(@PathParam("companyId")int companyId){
		return employeesManager.getAllEmployees(companyId);
	}
	
	@GET
	@Path("averageAge/{companyId}/")
	@Produces(MediaType.APPLICATION_JSON)
	public float getAgeAverage(@PathParam("companyId")int companyId){
		return employeesManager.getEmployeesAverageAge(companyId);
	}
}
