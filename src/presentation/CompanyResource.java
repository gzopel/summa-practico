package presentation;

import java.util.Collection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import logic.CompanyManager;
import domain.Company;

@RequestScoped
@Path("/companies/")
public class CompanyResource {
	@Inject
	private CompanyManager companyManager;
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Company> getAllEmployees(){
		return companyManager.getCompanies();
	}
}
