package presentation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import logic.EmployeeManager;
import domain.Designer;
import domain.Developer;
import domain.Employee;

@RequestScoped
@Path("/employee/")
public class EmployeeResource{

	@Inject
	private EmployeeManager employeeManager;

	@GET
	@Path("{id}/")
	@Produces(MediaType.APPLICATION_JSON)
	public Employee getEmployee(@PathParam("id")int id) {	
		return employeeManager.getEmployeeById(id);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void addEmployee(Employee e ) {
		employeeManager.addEmployee(e);
	}

}
