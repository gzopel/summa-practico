package exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class InvalidEmployeeException extends WebApplicationException {
		private static final long serialVersionUID = -6147597856029596017L;
		public InvalidEmployeeException(String message) {
	         super(Response.status(Response.Status.BAD_REQUEST)
	             .entity(message).type(MediaType.TEXT_PLAIN).build());
	     }
	
}
