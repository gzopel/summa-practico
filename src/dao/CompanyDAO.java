package dao;

import java.util.Collection;
import java.util.HashMap;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import domain.Company;

@Named
@ApplicationScoped
public class CompanyDAO {
	HashMap<Integer,Company> companies;
	
	CompanyDAO(){
		this.companies = new HashMap<Integer,Company>();
		this.companies.put(1,new Company(1,"Super Compu Mega Red"));
		this.companies.put(2,new Company(2,"Curry Code Factory"));
		this.companies.put(3,new Company(3,"Freelancers United"));
	}
	
	public Company getCompany(int id){
		if (companies.containsKey(id))
			return companies.get(id);
		return null;
	}
	
	public Collection<Company> getCompanies(){
		return companies.values();
	}
}
