package dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import domain.Designer;
import domain.Developer;
import domain.Employee;

@Named
@ApplicationScoped
public class EmployeeDAO {

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	static private class EmployeeMap { 
		private Hashtable<Integer,Employee> employees = new Hashtable<Integer,Employee>();
		
		public Employee get(int id) {
			if (employees.containsKey(id))
				return employees.get(id);
			return null;
		}

		public Collection<Employee> getAll() {
			return employees.values();
		}

		public boolean contains(Employee e) {
			return employees.containsKey(e.getId());
		}

		public void add(Employee e) {
			employees.put(e.getId(),e);
		}
	}
	
	private EmployeeMap table;
	private String filePath;
	
	public EmployeeDAO() {
		Properties prop = new Properties();
		InputStream input = EmployeeDAO.class.getResourceAsStream("dao.properties");
		try {
			prop.load(input);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally{
			try {
				input.close();
			} catch (IOException e) {
			}
		}
		filePath = prop.getProperty("employees_file_path");		
		try{
		    JAXBContext jaxbContext = JAXBContext.newInstance(EmployeeMap.class,Designer.class,Developer.class);
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		    table = (EmployeeMap) jaxbUnmarshaller.unmarshal( new File(this.filePath) );
		} catch (JAXBException ex){
			throw new RuntimeException(ex);
		} 
	}
	
	
	public Collection<Employee> getAllEmployees() {
		return table.getAll();
	}

	public Employee getEmployeeById(int id) {
		return table.get(id);
	}

	public void addEmployee(Employee e) {
		table.add(e);
		
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(this.filePath);
			Marshaller marshaller = JAXBContext.newInstance(EmployeeMap.class,Designer.class,Developer.class).createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(table,outputStream);
		} catch (JAXBException ex){
			//throw new RuntimeException(ex);
		} catch (FileNotFoundException ex){
			//throw new RuntimeException(ex);
		}finally {
			if (outputStream != null){
			    try {
					outputStream.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public boolean exists(Employee e) {
		if (table.contains(e)){
			return true;
		}
		return false;
	}
}
