package domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Designer extends Employee {
	private static final long serialVersionUID = 1051032295381355588L;
	private String designerType;

	public Designer(){
		super();
	}
	
	public Designer(int id, String firstName, String lastName, int age,String designerType, int companyId) {
		super(id,firstName,lastName,age,companyId);
		this.designerType=designerType;
	}

	public String getDesignerType() {
		return designerType;
	}

	public void setDesignerType(String designerType) {
		this.designerType = designerType;
	}
	
}
