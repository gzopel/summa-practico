package domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Developer extends Employee {
	private static final long serialVersionUID = 5586045921891191664L;
	protected String programingLanguage;

	public Developer(){
		super();
	}
	
	public Developer(int id, String firstName, String lastName, int age,String progamingLanguage,int companyId) {
		super(id,firstName,lastName,age,companyId);
		this.programingLanguage=progamingLanguage;
	}

	public String getProgamingLanguage() {
		return programingLanguage;
	}

	public void setProgamingLanguage(String language) {
		this.programingLanguage = language;
	}
	
}