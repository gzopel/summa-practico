package domain;

import java.io.Serializable;

public class Company implements Serializable{

	private static final long serialVersionUID = -3529689250637508686L;

	private int id;
	private String name;

	public Company(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Company(){}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	
}
