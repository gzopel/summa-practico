package domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;


@JsonTypeInfo(  
	    use = JsonTypeInfo.Id.NAME,  
	    include = JsonTypeInfo.As.PROPERTY,  
	    property = "type")    
@JsonSubTypes({  
	@JsonSubTypes.Type(value = Designer.class,name="designer"),  
	@JsonSubTypes.Type(value = Developer.class,name="developer") }) 
@XmlTransient 
@XmlAccessorType(XmlAccessType.NONE)
public abstract class Employee implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected int id;
	protected int companyId; //company FK
	protected String firstName;
	protected String lastName;
	protected int age;

	public Employee(){}
			
    public Employee(int id, String firstName, String lastName, int age, int companyId) {
		this.id = id;
		this.companyId=companyId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getCompanyId() {
		return companyId;
	}
	public void seCompanytId(int companyId) {
		this.companyId = companyId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

}
