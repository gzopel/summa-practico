package logic;

import java.util.Collection;
import java.util.LinkedList;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import dao.EmployeeDAO;
import domain.Employee;


@Named
@RequestScoped
public class EmployeesManager{

	@Inject
	EmployeeDAO dao;
	

	public Collection<Employee> getAllEmployees(int companyId) {
		Collection<Employee> companyEmployees = new LinkedList<Employee>();
		for (Employee e : dao.getAllEmployees())
			if(e.getCompanyId()==companyId)
				companyEmployees.add(e);
		
		return companyEmployees;
	}


	public float getEmployeesAverageAge(int companyId) {
		float total=0;
		Collection<Employee> employees = this.getAllEmployees(companyId);
		
		for(Employee e : employees)
				total += e.getAge();

		if (!employees.isEmpty())
			total/=employees.size();
		
		return total;
	}

}
