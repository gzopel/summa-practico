package logic;

import java.util.Collection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import dao.CompanyDAO;
import domain.Company;

@Named
@RequestScoped
public class CompanyManager {

	@Inject
	CompanyDAO dao;
	
	public Collection<Company> getCompanies(){
		return dao.getCompanies();
	} 
}
