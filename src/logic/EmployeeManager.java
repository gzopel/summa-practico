package logic;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import dao.EmployeeDAO;
import domain.Employee;
import exceptions.InvalidEmployeeException;

@Named
@RequestScoped
public class EmployeeManager {

	@Inject
	EmployeeDAO dao;
	
	public Employee getEmployeeById(int id) {
		if (id<0)
			throw new InvalidEmployeeException("Id should be positive");
				
		return dao.getEmployeeById(id);
	}

	public void addEmployee(Employee e) {
		if (e.getId()<0)
			throw new InvalidEmployeeException("Id should be positive");
			
		if (dao.exists(e)){
			throw new RuntimeException();
		}
		
		dao.addEmployee(e);
	}

}
